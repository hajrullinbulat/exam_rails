class TasksController < ApplicationController
  before_action :set_task, only: [:update, :destroy]
  before_action :authenticate_user!

  def index
    @tasks = current_user.tasks
  end

  # GET /tasks/1
  def show
    begin
      task = Task.find(params[:id])
      if task.user == current_user
        @task = task
        @comments = task.comments.sort_by {|comment| comment.created_at}.reverse
        render :show
      else
        redirect_back fallback_location: tasks_path, alert: 'Permissions denied'
      end
    rescue ActiveRecord::RecordNotFound => e
      redirect_back fallback_location: tasks_path, alert: 'Task not found'
    end
  end


  def create
    @task = Task.new(task_params)
    @task.user = current_user
    @task.status = "open"
    respond_to do |format|
      format.html { redirect_to tasks_path }
      if @task.save
        format.js
      else
        format.js { render "layouts/error.js"}
      end
    end
  end

  def update
    @task.update(task_params)

    respond_to do |format|
      format.html {redirect_to tasks_path}
      format.js
    end
  end

  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.html {redirect_to tasks_path}
      format.js
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:label, :description, :user_id, :status, :project_id, :id)
  end
end
