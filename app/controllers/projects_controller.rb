class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /projects
  def index
    @projects = current_user.projects
  end

  # POST /projects
  def create
    @project = Project.new(project_params)
    @project.users << current_user

    respond_to do |format|
      format.html { redirect_to projects_path }
      if @project.save
        format.js
      else
        format.js { render "layouts/error.js"}
      end
    end

  end

  # DELETE /projects/1
  def destroy
    @project = Project.find(params[:id])
    @project.destroy

    respond_to do |format|
      format.html { redirect_to projects_path }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def project_params
      params.require(:project).permit(:label, :description)
    end
end
