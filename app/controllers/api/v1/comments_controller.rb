module Api
  module V1
    class CommentsController < Api::V1::ApplicationController

      def create
        comment = Comment.new(comments_params)
        comment.user = current_user
        if comment.save
          render json: CommentSerializer.new(comment)
        else
          render json: comment.errors, status: :unprocessable_entity
        end
      end

      def destroy
        comment = current_user.comments.find_by_id(params[:id])
        if comment
          comment.destroy
          render json: {:success => "comment deleted"}.to_json
        else
          render json: {:error => "comment not found"}.to_json, status: :not_found
        end
      end

      private
      def comments_params
        params.require(:comment).permit(:text, :task_id)
      end

    end
  end
end
