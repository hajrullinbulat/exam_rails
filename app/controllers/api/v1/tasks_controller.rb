module Api
  module V1
    class TasksController < Api::V1::ApplicationController

      def show
        task = Task.find_by_id(params[:id])
        if task
          render json: TaskWithCommentsSerializer.new(task), include: 'comments.user'
        else
          render json: {:error => "task not found"}.to_json, status: :not_found
        end
      end

      def show_count
        task = Task.select("tasks.*, COUNT(comments.id) comments_count")
                   .joins("LEFT OUTER JOIN comments ON comments.task_id = tasks.id")
                   .group("tasks.id")
                   .find_by_id(params[:id])
        if task
          render json: TaskWithCommentsCountSerializer.new(task)
        else
          render json: {:error => "task not found"}.to_json, status: :not_found
        end
      end

      def create
        task = Task.new(task_params)
        task.user = current_user
        task.status = "open"
        if task.save
          render json: TaskSerializer.new(task)
        else
          render json: task.errors, status: :unprocessable_entity
        end
      end

      def destroy
        task = Task.find_by_id(params[:id])
        if task
          task.destroy
          render json: {:success => "task deleted"}.to_json
        else
          render json: {:error => "task not found"}.to_json, status: :not_found
        end
      end

      private
      def task_params
        params.require(:task).permit(:label, :description, :project_id)
      end

    end
  end
end
