module Api
  module V1
    class ProjectsController < Api::V1::ApplicationController

      def index
        render json: projects
      end

      def show
        project = Project.find_by_id(params[:id])
        if project
          render json: ProjectWithTasksSerializer.new(project)
        else
          render json: {:error => "project not found"}.to_json, status: :not_found
        end
      end

      def show_count
        project = Project.select("projects.*, COUNT(tasks.id) tasks_count")
                      .joins("LEFT OUTER JOIN tasks ON tasks.project_id = projects.id")
                      .group("projects.id")
                      .find_by_id(params[:id])
        if project
          render json: ProjectWithTasksCountSerializer.new(project)
        else
          render json: {:error => "project not found"}.to_json, status: :not_found
        end
      end

      def create
        project = Project.new(project_params)
        project.users << current_user
        if project.save
          render json: ProjectSerializer.new(project)
        else
          render json: project.errors, status: :unprocessable_entity
        end
      end

      def destroy
        project = current_user.projects.find_by_id(params[:id])
        if project
          if project.tasks.empty?
            project.destroy
            render json: {:success => "project deleted"}.to_json
          else
            render json: {:warning => "there are tasks of this project - cannot be deleted"}.to_json
          end
        else
          render json: {:error => "project not found"}.to_json, status: :not_found
        end
      end

      private
      def project_params
        params.require(:project).permit(:label, :description)
      end

      def projects
        projects = current_user.projects
        projects.map {|project| ProjectSerializer.new(project)}
      end

    end
  end
end
