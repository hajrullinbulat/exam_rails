class ProjectWithTasksCountSerializer < ActiveModel::Serializer
  attributes :id, :label, :description, :tasks_count, :created_at
end