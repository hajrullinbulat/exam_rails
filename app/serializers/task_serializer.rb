class TaskSerializer < ActiveModel::Serializer
  attributes :id, :label, :description, :status, :created_at
end
