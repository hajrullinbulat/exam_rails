class ProjectWithTasksSerializer < ActiveModel::Serializer
  attributes :id, :label, :description, :created_at

  has_many :tasks, serializer: TaskSerializer
end