class TaskWithCommentsSerializer < ActiveModel::Serializer
  attributes :id, :label, :description, :status, :created_at

  has_many :comments, serializer: CommentSerializer
end
