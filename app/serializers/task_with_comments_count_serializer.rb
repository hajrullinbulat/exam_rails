class TaskWithCommentsCountSerializer < ActiveModel::Serializer
  attributes :id, :label, :description, :status, :comments_count, :created_at
end