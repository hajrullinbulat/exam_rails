class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :label, :description, :created_at
end