function onStatusChanged(id, value) {
    $.ajax({
        url: '/tasks/' + id,
        method: 'PUT',
        dataType: 'JSON',
        data: {task: {status: value}}
    })
}