//= require rails-ujs
//= require jquery
//= require bootstrap-sprockets
//= require_tree .

$.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    }
});
