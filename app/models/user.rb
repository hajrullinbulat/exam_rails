class User < ApplicationRecord
  devise :database_authenticatable,
         :registerable,
         :rememberable,
         :validatable,
         :timeoutable

  include DeviseTokenAuth::Concerns::User

  has_many :tasks, dependent: :destroy
  has_many :comments, dependent: :destroy

  has_and_belongs_to_many :projects

  after_create :generate_token

  def generate_token
    update_attribute("token", SecureRandom.hex)
  end
end
