class Project < ApplicationRecord

  validates :label, :description, presence: true

  has_many :tasks, dependent: :destroy

  has_and_belongs_to_many :users, optional: true

end
