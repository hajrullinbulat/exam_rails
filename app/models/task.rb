class Task < ApplicationRecord
  validates :label, :status, :description, :project_id, :user_id, presence: true

  has_many :comments, dependent: :destroy

  belongs_to :user
  belongs_to :project

end
