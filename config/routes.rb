Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  resources :comments
  resources :tasks
  resources :projects

  devise_for :users

  namespace :api do
    namespace :v1 do
      mount_devise_token_auth_for 'User', at: 'auth'
      resources :tasks, only: %i(show create destroy)
      resources :projects, only: %i(index show create destroy)
      resources :comments, only: %i(create destroy)
      get 'projects/tasks-count/:id' => "projects#show_count"
      get 'tasks/comments-count/:id' => "tasks#show_count"
    end
  end

  root(to: "index#index")
end
