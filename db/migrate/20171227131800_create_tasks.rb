class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :label, null: false
      t.string :description, null: false
      t.string :status, null: false
      t.integer :user_id
      t.integer :project_id, null: false

      t.timestamps
    end
  end
end
