class DeviseCreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :token
      t.boolean :admin

      t.string :provider, :null => false, :default => "email"
      t.string :uid, :null => false, :default => ""

      t.string :email,              null: false, default: ""

      ## Database authenticatable
      t.string :encrypted_password, null: false, default: ""

      ## Rememberable
      t.datetime :remember_created_at

      ## Tokens
      t.json :tokens

      t.timestamps null: false
    end

    add_index :users, :email,                unique: true
  end
end
