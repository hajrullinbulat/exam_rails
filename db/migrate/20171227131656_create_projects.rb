class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :label, null: false
      t.string :description, null: false

      t.timestamps
    end
  end
end
